FROM golang:1.13 AS builder
WORKDIR /go/src/app
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build

# NOTE(mnaser): https://github.com/containers/skopeo/issues/991
# FROM scratch
FROM debian:stable-slim
COPY --from=builder /go/src/app/node-labeler /node-labeler
ENTRYPOINT ["/node-labeler"]
