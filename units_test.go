package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func assertCheckComputeNode(t *testing.T, label string, expected bool) {
	res := CheckComputeNode(label)
	assert.Equal(t, expected, res)
}

func TestCheckComputeSubstringWithkvmHostname(t *testing.T) {
	hostname := string("kvm10.specterops.iad1.vexxhost.net")
	expected := bool(true)
	assertCheckComputeNode(t, hostname, expected)

}

func TestCheckComputeNodeWithcomputeHostname(t *testing.T) {
	hostname := string("compute.specterops.iad1.vexxhost.net")
	expected := bool(true)
	assertCheckComputeNode(t, hostname, expected)

}

func TestCheckComputeNodeWithNoneComputeHostname(t *testing.T) {
	hostname := string("ctl1.specterops.iad1.vexxhost.net")
	expected := bool(false)
	assertCheckComputeNode(t, hostname, expected)

}
